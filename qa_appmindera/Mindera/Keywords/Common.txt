*** Settings ***
Resource          ../Settings/global.txt
Library           BuiltIn
Library           Collections
Library           Dialogs
Library           OperatingSystem
Library           Selenium2Library
Library           String
Library           DateTime
Library			  ../Library/CommonLibraryPython.py

*** Keywords ***
Open Application
    [Arguments]    ${url}
    ${index}    Run Keyword If    '${BROWSER}'=='gc'    Create Chrome Driver    ${url}
    Set Window Size    1920    1080
    [Return]    ${index}
    
Create Chrome Driver
    [Arguments]    ${url}
    ${chrome_options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys
    Call Method    ${chrome_options}    add_argument    --disable-extensions
    Call Method    ${chrome_options}    add_argument    --disable-popup-blocking
    Call Method    ${chrome_options}    add_argument    --disable-infobars
    Call Method    ${chrome_options}    add_argument    --headless
    ${index}    Create Webdriver    Chrome    chrome_options=${chrome_options}
    Go To    ${url}
    [Return]    ${index}
    