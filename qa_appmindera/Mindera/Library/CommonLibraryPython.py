import Selenium2Library
import re

import selenium as Selenium

from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver import ActionChains
from selenium import webdriver

from robot.libraries.BuiltIn import BuiltIn
from robot.utils import error


class CommonLibraryPython(object):

    def sort_up_to_down(self):
        selenium = BuiltIn().get_library_instance('Selenium2Library')
        driver = selenium._current_browser()
        action = ActionChains(driver)
        # Get list of Elements
        table_list = driver.find_element_by_xpath("//div[@id='app']//ul/li").text
        index = len(table_list)
        while index >= 0:
            for i in range(1, index):
                # Get Source and Target elements by Xpath
                source_element = driver.find_element(By.XPATH, ".//div[@id='app']//ul/li["+str(i) + "]")
                target_element = driver.find_element(By.XPATH, ".//div[@id='app']//ul/li["+str(i+1) + "]")
                # Get name of Source and Target elements
                source_text = driver.find_element(By.XPATH, ".//div[@id='app']//ul/li["+str(i) + "]").text
                target_text = driver.find_element(By.XPATH, ".//div[@id='app']//ul/li["+str(i+1) + "]").text
                # Get number of source and target element
                source = re.findall('\d', source_text)
                target = re.findall('\d', target_text)
                # Bubble Sort Algorithm used to sort the items
                if source > target:
                    action.drag_and_drop(source_element, target_element).perform()
                    action.drag_and_drop(target_element, source_element).perform()
            index -= 1

    def sort_down_to_up(self):
        selenium = BuiltIn().get_library_instance('Selenium2Library')
        driver = selenium._current_browser()
        action = ActionChains(driver)
        table_list = driver.find_element_by_xpath("//div[@id='app']//ul/li").text
        index = len(table_list)
        while index >= 0:
            for i in range(index, 1, -1):
                # Get Source and Target elements by Xpath
                source_element = driver.find_element(By.XPATH, ".//div[@id='app']//ul/li[" + str(i) + "]")
                target_element = driver.find_element(By.XPATH, ".//div[@id='app']//ul/li[" + str(i - 1) + "]")
                # Get name of Source and Target elements
                source_text = driver.find_element(By.XPATH, ".//div[@id='app']//ul/li[" + str(i) + "]").text
                target_text = driver.find_element(By.XPATH, ".//div[@id='app']//ul/li[" + str(i - 1) + "]").text
                # Get number of source and target element
                source = re.findall('\d', source_text)
                target = re.findall('\d', target_text)
                # Bubble Sort Algorithm used to sort the item
                if source < target:
                    action.drag_and_drop(source_element, target_element).perform()
                    action.drag_and_drop(target_element, source_element).perform()
            index -= 1
